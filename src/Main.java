import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        String input = "(x+`x)*y";

        Obj[] objs = new Obj[input.length()];
        for(int i = 0; i < input.length(); i++) {
            char symbol = input.charAt(i);
            switch (symbol) {
                case '(':
                    objs[i] = new OpenBr();
                    break;
                case ')':
                    objs[i] = new CloseBr();
                    break;
                case '`':
                    objs[i] = new Inverce();
                    break;
                case '*':
                    objs[i] = new Intercept();
                    break;
                case '+':
                    objs[i] = new Union();
                    break;
                default:
                    objs[i] = new Variable(symbol);
            }
        }

    }

    public static Obj[] calc(Obj[] objs) {
        List<Pair> positions = getPositionsBr(objs);

        if (positions.size() == 0) {
            //упроститить..
            //return Obj[]
        }

        for (Pair pos : positions) {
            Obj[] tmp = Arrays.copyOfRange(objs, pos.x + 1, pos.y - 1);
            Obj[] calcVar = calc(tmp);
            //заменить с pos.x по pos.y на calcVar
            Obj[] newObjs = new Obj[calcVar.length + objs.length - (pos.y - pos.x)];
        }

        //упростить

        //return
    }

    public static boolean isValid(Obj[] objs) {
        int count = 0;
        int idxOpen = 0;
        int idxClose = 0;
        for (int i = 0; i < objs.length; i++) {
            if(objs[i].isType("OpenBr")) {
                count++;
                idxOpen = i;
            }
            if(objs[i].isType("CloseBr")) {
                count--;
                if (count < 0) {
                    return false;
                }
                idxClose = i;

                if (idxClose == idxOpen + 1) {
                    return false;
                }
            }

        }
        return true;
    }

    public static List<Pair> getPositionsBr(Obj[] objs) {
        List<Pair> positions = new ArrayList<>();

        int count = 0;
        int idxOpen = 0;
        int idxClose = 0;
        for (int i = 0; i < objs.length; i++) {
            if(objs[i].isType("OpenBr")) {
                if(count == 0) {

                    idxOpen = i;
                }
                count++;
            }
            if(objs[i].isType("CloseBr")) {
                count--;
                idxClose = i;
            }

            if(count == 0) {
                positions.add(new Pair(idxOpen, idxClose));
            }
        }

        return positions;
    }
}

class Pair {
    int x;
    int y;

    Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
