public class Variable implements Obj{
    private char symbol;

    public Variable(char symbol) {
        this.symbol = symbol;
    }

    @Override
    public boolean isType(String type) {
        if(type.equals("Variable")) {
            return true;
        }
        return false;
    }
}
